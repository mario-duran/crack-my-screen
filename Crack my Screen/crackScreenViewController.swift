//
//  crackScreenViewController.swift
//  Crack my Screen
//
//  Created by Mario Duran on 11/9/15.
//  Copyright © 2015 Orinoco Systems LLC. All rights reserved.
//

import UIKit
import iAd
import AVFoundation

class crackScreenViewController: UIViewController, AVAudioPlayerDelegate {
    
    @IBOutlet weak var crackImageView: UIImageView!
    
    var loadImage: UIImage!
    var barSwitch: Int = 1
    var theCrack :UIImageView!
    var cracked = false
    var audioPlayer: AVAudioPlayer!
    var forceValue: CGFloat = 0.0

    @IBOutlet weak var toolBar: UIToolbar!
    
    @IBOutlet var longPressCrack: UILongPressGestureRecognizer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if loadImage != nil {
            
            //set the player
            audioPlayer = setupAudioPlayerWithFile("crackGlass", type: "mp3")
            audioPlayer.delegate = self
            audioPlayer.prepareToPlay()
            
            
            crackImageView.image = loadImage
            
            if(crackImageView.image?.size.height <= crackImageView.image?.size.width) {
                crackImageView.contentMode = UIViewContentMode.ScaleAspectFill
            } else {
                crackImageView.contentMode = UIViewContentMode.ScaleAspectFit
            }

            
            theCrack = UIImageView(frame:CGRectMake(0, 0, 681, 1504));
            theCrack.image = UIImage(named:"masterCrack")
            theCrack.alpha = 0
            theCrack.hidden = true
            theCrack.contentMode = UIViewContentMode.ScaleAspectFill
            self.view.addSubview(theCrack)
    
            
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func tapDisplayMenu(sender: AnyObject) {
        
        var thisAlpha:CGFloat = 0.0
        
        if barSwitch == 1 {
            thisAlpha = 1
            barSwitch = 0
        } else {
            thisAlpha = 0
            barSwitch = 1
        }
        
        toolBar.layer.zPosition = 99
        
        
        UIView.animateWithDuration(0.25, animations: { self.toolBar.alpha = thisAlpha})
        
    }
    
    func cleanUp() {
        theCrack.hidden = true
        theCrack.alpha = 0
        cracked = false
        
        tapDisplayMenu(self)
    }
    
    @IBAction func clearCrack(sender: UIBarButtonItem) {
        cleanUp()
    }
    
    
    @IBAction func longPressCrack(sender: UILongPressGestureRecognizer) {
        
        if !cracked {
            if(is3DTouchAvailable()) {
                if(forceValue >= 0.5) {
                    audioPlayer.play()
                    CrackScreen()
                }
            } else {
                audioPlayer.play()
                CrackScreen()
            }
            
        }
        
    }
    
    func CrackScreen() {
        
        let Position = self.longPressCrack.locationInView(self.view)
        let realX = Position.x - (theCrack.frame.size.width / 2)
        let realY = Position.y - (theCrack.frame.size.height / 2)
        
        let H:CGFloat = 1504.0
        let W:CGFloat = 681.0
        

        theCrack.frame = CGRectMake(realX, realY, W, H)
        theCrack.hidden = false
        cracked = true
        
        NSTimer.scheduledTimerWithTimeInterval(NSTimeInterval(0.5), target: self, selector: "showCrack", userInfo: nil, repeats: false)
        
    }
    
    func showCrack() {
        UIView.animateWithDuration(0.25, animations: { self.theCrack.alpha = 0.99})
    }
    
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        let destination = segue.destinationViewController as UIViewController
        
        let bckBtn = (sender is UIBarButtonItem ? true : false)
        
        if(bckBtn) { //ONLY IF THE BACK BUTTON IS PRESSED
            if(sender?.tag == 1) {
                destination.interstitialPresentationPolicy = ADInterstitialPresentationPolicy.Automatic
            }
        }
        
    }
    
    
    func setupAudioPlayerWithFile(file:NSString, type:NSString) -> AVAudioPlayer  {
        let path = NSBundle.mainBundle().pathForResource(file as String, ofType: type as String)
        let url = NSURL.fileURLWithPath(path!)
        var audioPlayer:AVAudioPlayer?
        
        do {
            try audioPlayer = AVAudioPlayer(contentsOfURL: url)
        } catch {
            print("NO AUDIO PLAYER")
        }

        return audioPlayer!
    }
    
    func is3DTouchAvailable() -> Bool
    {
        if self.traitCollection.forceTouchCapability == UIForceTouchCapability.Available
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        for touch in touches {
            //print("Begin at: \(touch.timestamp)")
            self.forceValue = touch.force
        }
        
    }
    
    override func touchesMoved(touches: Set<UITouch>, withEvent event: UIEvent?) {
        
        for touch in touches {
            //print("Moved at: \(touch.timestamp)")
            self.forceValue = touch.force
        }
        
    }
    
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        for touch in touches {
            //print("Ended at: \(touch.timestamp)")
            self.forceValue = touch.force
        }
        
    }



}
