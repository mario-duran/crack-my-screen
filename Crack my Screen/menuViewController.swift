//
//  menuViewController.swift
//  Crack my Screen
//
//  Created by Mario Duran on 11/9/15.
//  Copyright © 2015 Orinoco Systems LLC. All rights reserved.
//

import UIKit
import iAd

class menuViewController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {

    var imagePicker = UIImagePickerController()
    var selectedImage: UIImage!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func pickImage(sender: UIButton) {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.SavedPhotosAlbum){
            
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.SavedPhotosAlbum;
            imagePicker.allowsEditing = false
            
            self.presentViewController(imagePicker, animated: true, completion: nil)
        }

    }
    
    func imagePickerController(picker: UIImagePickerController!, didFinishPickingImage image: UIImage!, editingInfo: NSDictionary!){
        self.dismissViewControllerAnimated(true, completion: { () -> Void in
            self.selectedImage = image
            self.doSegue()
        })
    }

    
    func doSegue() {
        performSegueWithIdentifier("crackSegue", sender: self)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == "crackSegue" {
            let destination = segue.destinationViewController as! crackScreenViewController
            destination.loadImage = selectedImage
        }
        
    }


    @IBAction func selectDefault(sender: UIButton) {
        
        if(self.view.frame.width == 320) {
            selectedImage = UIImage(named: "defaultCrack640" )

        } else {
            selectedImage = UIImage(named: "defaultCrack" )

        }
        doSegue()
    
    }


}
