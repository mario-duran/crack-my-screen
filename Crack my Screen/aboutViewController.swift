//
//  aboutViewController.swift
//  Crack my Screen
//
//  Created by Mario Duran on 11/12/15.
//  Copyright © 2015 Orinoco Systems LLC. All rights reserved.
//

import UIKit
import iAd

class aboutViewController: UIViewController {

    @IBOutlet weak var thisWebView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let theHtml = NSBundle.mainBundle().URLForResource("about", withExtension: "html")
        
        let requestObj = NSURLRequest(URL: theHtml!)
        
        thisWebView.loadRequest(requestObj)

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func gotoOrinoco(sender: UITapGestureRecognizer) {
        UIApplication.sharedApplication().openURL(NSURL(string:"http://www.orinoco-systems.com/")!)
    }


}
