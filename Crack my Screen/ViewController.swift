//
//  ViewController.swift
//  Crack my Screen
//
//  Created by Mario Duran on 10/25/15.
//  Copyright © 2015 Orinoco Systems LLC. All rights reserved.
//

import UIKit
import iAd
import CoreData

class ViewController: UIViewController {

    @IBOutlet weak var pickScene: UIImageView!
    @IBOutlet weak var descLabel: UILabel!
    
    @IBOutlet weak var stepsDisplay: UIImageView!
    @IBOutlet weak var skipButton: UIButton!
    
    
    var steps: Int = 0
    var actualScene: UIImageView!
    var labelsArr = ["Pick a picture", "Make it Crack!", "Start Over"]
    
    var imageArray = [UIImage(named: "Pick" ), UIImage(named: "Crack" ), UIImage(named: "Refresh" )]
    
    var stepsArray = [UIImage(named: "step_one" ), UIImage(named: "step_two" ), UIImage(named: "step_three" )]
    
    var mainRect: CGRect!
    var mainWidth: CGFloat!
    
    var mainData = [NSManagedObject]()
    
    var firstLoad = true
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        //1
        let appDelegate =
        UIApplication.sharedApplication().delegate as! AppDelegate
        
        let managedContext = appDelegate.managedObjectContext
        
        //2
        let fetchRequest = NSFetchRequest(entityName: "MainInfo")
        
        //3
        do {
            let results =
            try managedContext.executeFetchRequest(fetchRequest)
            mainData = results as! [NSManagedObject]
            
            if(mainData.count > 0) {
                self.firstLoad = (mainData[0].valueForKey("firstLoad") as? Bool)!
            }
            
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        // Do any additional setup after loading the view, typically from a nib.
        
        initScenes()
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func initScenes() {
        descLabel.text = labelsArr[0]
        mainRect = pickScene.frame
        mainWidth = view.frame.width
        
    }
    
    @IBAction func moveLeft(sender: UISwipeGestureRecognizer) {
        
        if(steps < 3) {
            steps = steps + 1
            moveStep(steps, effect: "swipeL")
        }
    }
    
    @IBAction func moveRight(sender: UISwipeGestureRecognizer) {
        
        if(steps > 0) {
            steps = steps - 1
            moveStep(steps, effect: "swipeR")
        }
    }
    
    @IBAction func tapMove(sender: UITapGestureRecognizer) {
        steps = steps + 1
        moveStep(steps, effect: "tap")
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let destination = segue.destinationViewController
            as UIViewController
        
        let skipBtn = (sender is UIButton ? true : false)
        
        if(skipBtn) {
            if(sender?.tag == 1) && (firstLoad == true) {
                destination.interstitialPresentationPolicy = ADInterstitialPresentationPolicy.Automatic
            }
        }
        
    }
    
    func moveStep(stepIn: Int, effect: String) {
        
        if(stepIn <= 2) {

            pickScene.alpha = 0
            pickScene.image = imageArray[stepIn]
            descLabel.text = labelsArr[stepIn]
            stepsDisplay.image = stepsArray[stepIn]
            
            if(effect == "tap") {
                UIView.animateWithDuration(0.75, animations: { self.pickScene.alpha = 1})
            } else {
                var thisX = pickScene.frame.minX
                let thisY = pickScene.frame.minY
                let thisW = pickScene.frame.width
                let thisH = pickScene.frame.height
                
                if(effect == "swipeR") {
                    thisX = thisX - mainWidth
                } else {
                    thisX = thisX + mainWidth
                }
                
                pickScene.frame = CGRect(x: thisX, y: thisY, width: thisW, height: thisH)
                pickScene.alpha = 1
                
                UIView.animateWithDuration(0.25, animations: { self.pickScene.frame = self.mainRect})
            }
            
           
        } else {
            //SAVE THE PASS
            
            if(mainData.count == 0) {
                
                let managedContext = AppDelegate().managedObjectContext
                let MainInfo = NSEntityDescription.insertNewObjectForEntityForName("MainInfo", inManagedObjectContext: managedContext)
                
                MainInfo.setValue(false, forKey: "firstLoad")
                
                do {
                    try managedContext.save()
                } catch {
                    fatalError("Failure to save context: \(error)")
                }
                
            }
            
            performSegueWithIdentifier("mainView", sender: self)
        }
        
        
    }
}

